import * as plugins from './smartparcel.plugins';

export class Parcel {
  private options: {
    from: string | string[],
    outputDir: string,
    outputFile: string
  } = {
    from: null,
    outputDir: null,
    outputFile: null,
  }
  private defaultOptions: ConstructorParameters<typeof plugins.parcel.Parcel>[ 0 ] = {
    defaultConfig: '@parcel/config-default',
    cacheDir: './.nogit/.parcelcache',
    defaultTargetOptions: {
      distDir: plugins.path.join(process.cwd(), './dist_watch'),
      publicUrl: '/',
      engines: {
        browsers: [ 'last 1 Chrome version' ]
      }
    },
    entries: './html/index.html',
    hmrOptions: {
      port: 3003
    },
    serveOptions: {
      port: 3002
    },
    shouldAutoInstall: true
  };
  public entryFiles: string | string[] = plugins.path.join(process.cwd(), './html/index.html');


  public async createOptions() {
    const returnOptions: ConstructorParameters<typeof plugins.parcel.Parcel>[ 0 ] = {
      ...this.defaultOptions,
      entries: this.options.from,
      defaultTargetOptions: {
        ...this.defaultOptions.defaultTargetOptions,
        distDir: this.options.outputDir
      }
    }
    return returnOptions;
  }

  /**
   * builds and serves
   */
  public async watchAndServe () {
    const bundler = new plugins.parcel.Parcel(await this.createOptions());

    let subscription = await bundler.watch((err, event) => {
      if (err) {
        // fatal error
        console.log(err);
        throw err;
      }
    
      if (event.type === 'buildSuccess') {
        let bundles = event.bundleGraph.getBundles();
        console.log(`✨ Built ${bundles.length} bundles in ${event.buildTime}ms!`);
      } else if (event.type === 'buildFailure') {
        console.log(event.diagnostics);
      }
    });
  }

  /**
   * just builds
   */
  public async build () {
    let bundler = new plugins.parcel.Parcel(await this.createOptions());
    
    try {
      let {bundleGraph, buildTime} = await bundler.run();
      let bundles = bundleGraph.getBundles();
      console.log(`✨ Built ${bundles.length} bundles in ${buildTime}ms!`);
    } catch (err: any) {
      console.log(err.diagnostics);
    }
  }

  constructor(fromArg: string | string[], outputDirArg: string, outputFileArg: string) {
    this.entryFiles = fromArg;
    this.options = {
      from: fromArg,
      outputDir: outputDirArg,
      outputFile: outputFileArg
    };
  }
}
