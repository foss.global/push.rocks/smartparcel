// node native
import * as path from 'path';

export { path };

// @pushrocks scope
import * as smartpath from '@pushrocks/smartpath';

export { smartpath };

// third party scope
import * as parcel from '@parcel/core';

export { parcel };
